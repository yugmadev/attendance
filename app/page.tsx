import { sql } from '@vercel/postgres';
import { Card, Title, Text } from '@tremor/react';
import Search from './search';
import UsersTable from './table';

interface User {
  id: number;
  name: string;
  regist: string;
  present: boolean;
  updated_at: Date;
}

async function getUsers(search: string) {
  const result = await sql`
    SELECT id, regist, name, present,updated_at 
    FROM users 
    WHERE name ILIKE ${'%' + search + '%'};
  `;

  return result.rows as User[]
}

export default async function IndexPage({
  searchParams
}: {
  searchParams: { q: string };
}) {
  const search = searchParams.q ?? '';
  const users: User[] = await getUsers(search);

  return (
    <main className="p-4 md:p-10 mx-auto max-w-7xl">
      <Title>Users</Title>
      <Text>A list of users retrieved from a Postgres database.</Text>
      <Search />
      <Card className="mt-6">
        <UsersTable users={users} />
      </Card>
    </main>
  );
}
