"use client"
import { sql } from '@vercel/postgres';

import {
  Table,
  TableHead,
  TableRow,
  TableHeaderCell,
  TableBody,
  TableCell,
  Text,
  Select,
  SelectItem
} from '@tremor/react';
import { Suspense } from 'react';

interface User {
  id: number;
  name: string;
  regist: string;
  present: boolean;
  updated_at: Date;
}

async function handleChange(val: string, id: number) {
  let ad = await fetch('/api/peserta', {
    method: "POST",
    body: JSON.stringify({ value: val, id: id }),
  })
  if (!ad.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error('Failed to fetch data')
  }
  const resp = await ad.json();
  return resp
}
export default function UsersTable({ users }: { users: User[] }) {
  let no = 1;
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableHeaderCell>No.</TableHeaderCell>
          <TableHeaderCell>No. Peserta</TableHeaderCell>
          <TableHeaderCell>Nama</TableHeaderCell>
          <TableHeaderCell>Status Kehadiran</TableHeaderCell>
          <TableHeaderCell>Terakhir Diubah</TableHeaderCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {users.map((user) => (
          <TableRow key={user.id}>
            <TableCell>{no++}</TableCell>
            <TableCell>{user.regist}</TableCell>
            <TableCell><Text>{user.name}</Text></TableCell>
            <TableCell>
              {/* <SelectPresent user={user} handleChange={handleChange} /> */}
              <Select defaultValue={user.present ? ("true") : ("false")} onValueChange={(selectedValue) => handleChange(selectedValue, user.id)}>
                <SelectItem value='true'><Text>Hadir</Text></SelectItem>
                <SelectItem value='false'><Text>Belum/Tidak Hadir</Text></SelectItem>
              </Select>
            </TableCell>
            <TableCell>
              <Text>
                <Suspense fallback={null}>
                  {user.updated_at.toLocaleString(undefined, { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' })}
                </Suspense>
              </Text>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}
